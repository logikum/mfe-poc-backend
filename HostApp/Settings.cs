﻿namespace HostApp
{
    public static class Settings
    {
        public static string JwtSecret = "This is a top secret string!";
        public static string DefaultCorsPolicy = "Armageddon";
    }
}
