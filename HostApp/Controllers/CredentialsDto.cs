﻿namespace HostApp.Controllers
{
    public class CredentialsDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
