﻿using System;

namespace HostApp.Controllers
{
    public class PatientDto
    {
        public Guid PatientID { get; set; }
        public string PatientName { get; set; }
    }
}
