﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace HostApp.Controllers
{
    [Route("api/host")]
    [ApiController]
    public class HostController : ControllerBase
    {
        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult UserLogin(
            [FromBody] CredentialsDto dto
            )
        {
            try
            {
                if (dto.Username.Length < 4 || dto.Password.Length < 4)
                    throw new ApplicationException("Invalid credentials.");

                // Authentication is successful, generate JWT token.
                string jwt = null;
                Guid patientID = Guid.NewGuid();
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(Settings.JwtSecret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, dto.Username),
                        new Claim("PatientID", patientID.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddHours(10),
                    SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(key),
                        SecurityAlgorithms.HmacSha256Signature
                        )
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                jwt = tokenHandler.WriteToken(token);

                // Return the JSON Web Token.
                return Ok(jwt);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new ApplicationException("An unexpected error occurred.");
            }
        }

        [Authorize]
        [HttpGet("patient/{patientID}")]
        public IActionResult GetPatient(
            Guid? patientID
            )
        {
            try
            {
                if (patientID.HasValue && patientID.Value != Guid.Empty)
                {
                    PatientDto dto = new PatientDto
                    {
                        PatientID = patientID.Value,
                        PatientName = "Kakuk Marci"
                    };
                    return Ok(dto);
                }
                else
                return NotFound();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new ApplicationException("An unexpected error occurred.");
            }
        }
    }
}
