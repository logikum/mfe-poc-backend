﻿using System.Security.Principal;

namespace EmbeddedApp.Controllers
{
    public class CustomPrincipal : IPrincipal
    {
        private CustomIdentity? _identity;

        public IIdentity? Identity { get { return _identity; } }
        public CustomIdentity? CustomIdentity { get { return _identity; } }

        public CustomPrincipal()
        { }

        public CustomPrincipal(
            CustomIdentity? identity
            )
        {
            _identity = identity;
        }

        public bool IsInRole(
            string role
            )
        {
            return false;
        }
    }
}
