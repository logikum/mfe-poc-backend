﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace EmbeddedApp.Controllers
{
    [Route("api/patient")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private string _jwt = null;
        private CustomPrincipal _principal = null;

        public string JWT
        {
            get { return _jwt; }
        }

        public CustomIdentity Identity
        {
            get { return _principal?.CustomIdentity; }
        }

        private void SetPrincipa(
            string patientToken
            )
        {
            if (_principal == null)
            {
                try
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHandler.ReadToken(patientToken) as JwtSecurityToken;

                    Guid patientID = Guid.Empty;
                    string patientName = securityToken.Claims
                        //.First(c => c.Type == ClaimTypes.Name)
                        .First(c => c.Type == "unique_name")
                        .Value;
                    if (Guid.TryParse(
                        securityToken.Claims.First(c => c.Type == "PatientID").Value,
                        out patientID
                       ))
                    {
                        CustomIdentity identity = new CustomIdentity(patientID, patientName);
                        _principal = new CustomPrincipal(identity);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        //[Authorize]
        [HttpGet("{patientToken}")]
        public async Task<IActionResult> GetPatient(
            string patientToken
            )
        {
            try
            {
                PatientDto dto = null;
                SetPrincipa(patientToken);

                if (Identity != null && Identity.IsAuthenticated)
                {
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("http://localhost:5000");
                    client.DefaultRequestHeaders.Add("Accept", "application/json");
                    client.DefaultRequestHeaders.Add("User-Agent", "Embedded Application");
                    client.DefaultRequestHeaders.Add("Authorization", $"Bearer { patientToken /*JWT*/ }");

                    string url = $"/api/host/patient/{ Identity.PatientID }";
                    //HttpResponseMessage response = client.GetAsync(url).Result;
                    HttpResponseMessage response = await client.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonString = await response.Content.ReadAsStringAsync();
                        var options = new JsonSerializerOptions
                        {
                            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        };
                        dto = JsonSerializer.Deserialize<PatientDto>(jsonString, options);
                    }
                }
                return Ok(dto);
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.Message);
                //throw new ApplicationException("An unexpected error occurred.");
                throw;
            }
        }
    }
}
