﻿using System;
using System.Security.Principal;

namespace EmbeddedApp.Controllers
{
    public class CustomIdentity : IIdentity
    {
        private string? _authenticationType;
        private bool _isAuthenticated;
        private string? _name;
        private Guid? _patientID;
        private string? _patientName;

        public string? AuthenticationType { get { return _authenticationType; } }
        public bool IsAuthenticated { get { return _isAuthenticated; } }
        public string? Name { get { return _name; } }
        public Guid? PatientID { get { return _patientID; } }
        public string? PatientName { get { return _patientName; } }

        public CustomIdentity()
        {
            _isAuthenticated = false;
        }

        public CustomIdentity(
            Guid? patientID,
            string? patientName
            )
        {
            if (patientID.HasValue && patientID.Value != Guid.Empty && !string.IsNullOrEmpty(patientName))
            {
                _authenticationType = "OpenAuth2";
                _isAuthenticated = true;
                _name = patientName;
                _patientID = patientID;
                _patientName = patientName;
            }
            else
            {
                _isAuthenticated = false;
            }
        }
    }
}
