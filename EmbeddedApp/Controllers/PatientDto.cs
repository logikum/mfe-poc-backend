﻿using System;

namespace EmbeddedApp.Controllers
{
    public class PatientDto
    {
        public Guid PatientID { get; set; }
        public string PatientName { get; set; }
    }
}
